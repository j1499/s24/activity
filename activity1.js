//1 Add the ff users
db.users.insertMany([
	{
		"firstName": "Diane",
		"lastName": "Murphy",
		"email": "dmurphy@mail.com",
		"isAdmin": false,
		"isActive": true
	},
	{
		"firstName": "Mary",
		"lastName": "Patterson",
		"email": "mpatterson@mail.com",
		"isAdmin": false,
		"isActive": true
	},
	{
		"firstName": "Jeff",
		"lastName": "Firrelli",
		"email": "jfirrelli@mail.com",
		"isAdmin": false,
		"isActive": true
	},
	{
		"firstName": "Gerard",
		"lastName": "Bondur",
		"email": "gbondur@mail.com",
		"isAdmin": false,
		"isActive": true
	},
	{
		"firstName": "Pamela",
		"lastName": "Castillo",
		"email": "pcastillo@mail.com",
		"isAdmin": true,
		"isActive": false
	},
	{
		"firstName": "George",
		"lastName": "Vanauf",
		"email": "gvanauf@mail.com",
		"isAdmin": true,
		"isActive": true
	}
]);

//2 add the following courses
db.courses.insertMany([
	{
		"name": "Professional Development",
		"price": 10000
	},
	{
		"name": "Business Processing",
		"price": 13000
	},
]);

//3 get the users who are not administrators
db.users.find({"isAdmin": false});

//4 get user IDs and add them as enrollees of the courses
db.courses.updateOne([
	{
		"name": "Professional Development"
	},
	{
		$set: {
			"enrollees": ObjectId("620cc5d812c053294a71c016"),
			"enrollees": ObjectId("620cc5d812c053294a71c017")
		}
	}
]);
